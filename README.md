﻿# My project's README

DeliveryBuddyApp Requirements:

<1> App source code for deliveryBuddyApp is available in DeliveryApp/DeliveryBuddyAppfiles/AppSourceCode

<2> Required php scripts for fetching data from database for this application are available in  DeliveryApp/DeliveryBuddyAppFiles/DatabaseScripts

<3> delivery app will fetch data from orders table, cook_person table, delivery_persons table. All of these tables need to be created in database.

<3.a> cook_person table will contain information about cooking person who cooked food for respective days. delivery_person table will contains information about all the delivery persons. orders table will be just copy checkout table will additonal information about delivery location and delivery person.

<3.b> cook_person table and delivery_persons table can be created from db-scripts.sql available in DeliveryApp/MoreMilagaWebAdminFiles/db-scripts.sql 

<3.c> orders tables will be created and updated automatically from DeliveryApp/MoreMilagaWebAdminFiles/assignToDelivery.php.

<3.d> But for orders tables to work you need to add 2 more fields (location, delivery_person) in the existing checkout table in database. In checkout table add 'location' field just after address field and also add 'delivery_person' field in checkout at the end after display field.







^
^

Updated moremilaga Admin pages requirements:

<1>Updated admin pages are available in DeliveryApp/MoreMilagaWebAdminFiles/

<2>In admin website(appsettings) menu bar has been modified. So you need to replace existing menu.html in appsettings with DeliveryApp/MoreMilagaWebAdminFiles/menu.html

<2.a> updated menu.html contains additional tabs (Allocate Orders, Add Cook, Delivery Analysis).

<2.b> Allocate order tab in menu.html is for assigning location(eg. mylapore,triplicane,alwarpet) and also assigning delivery person to given order.

<2.c> Add Cook tab in menu.html contains a form for filling information about cooking person who will cook food for given day.

<2.c> Delivery Analysis will give report about total deliveries made by delivery person or by location by setting dates.

<3> php script for Allocate Orders is avaialble in DeliveryApp/MoreMilagaWebAdminFiles/assignToDelivery.php

<3.a> In admin menu when you click on Allocate orders tab it will show you orders details from orders table in database.

<3.b> for assigning location and delivery person to given order in table, you need to click on checkbox and then select location and delivery person, then click send at bottom of table. It will update orders table by updating location and delivery person fields.

<3.c> For exporting order details click on export at bottom of orders information in Allocate order page.

<4> php script for Add Cook is available in DeliveryApp/MoreMilagaWebAdminFiles/addCook.php

<4.a> after adding cook and submitting form it will update cook_person table in database and assign current date to it. This cook_person table will be used in 'Pick Up' tab in delivery buddy app so that delivery person will know info about which cook and from where to pick the ordered food.

<5> php script for Delivery Analysis is available in DeliveryApp/MoreMilagaWebAdminFiles/delivery_analysis.php

<5.a> In admin when you click on Delivery Analysis tab it will show you a page where you can set start date and end date. After submitting dates it will show you information about total deliveries made by delivery person and also total deliveries made in locations